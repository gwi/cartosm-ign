CartOSM [cartOSM]
======================================================
Type de jeu pour integrer des cartes IGN dans Minetest
Subgame to integrate IGN's maps in the Minetest engine
======================================================

Pour utiliser ce subgame avec Minetest, mettre ce repertoire comme suit :
To use this subgame with the Minetest engine, insert this repository as :
	/games/cartOSM

Ce subgame est disponible sur :
This subgame can be found in :
	http://cartosm.git.lespetitsdebrouillards-na.org

Le jeu Minetest est disponible sur :
The Minetest engine can be found in :
	https://github.com/minetest/minetest/



Mods & modifications
--------------------

## FR ##
Avant de manipuler les mods, pensez a verifier les fichiers depends.txt de chacun.


* Mods de minetest_game

** Mods conserves
beds , boats , bones , bucket , carts , creative , doors , dye , farming , flowers , screwdriver , sethome , sfinv , stairs , vessels , walls , wool , xpanes

** Mods modifies
default
	Lave non-inflammable : commentaire des lignes 57 à 66 dans item_entity.lua
give_initial_stuff
	 Ajout de l'inventaire du depart (torches, craftguide, blocs, ...) de la ligne 14 a la ligne 26 dans init.lua
killme
	Traduction FR des messages dans init.lua

** Mods supprimes
fire


* Mods additifs

** Mods ajoutes
areas , arrow_signs , craftguide , display_modpack , extrahb , h2omes , homedecor , intlib , mapfix , mesecons , moreblocks , moreores , nyancat , polls , signs_lib , simple_skins , spectactor , streets , unifieddyes , unifieddyes_inventory , WorldEdit

** Mods modifies
markers
	Traduction FR dans areas.lua , init.lua , land_title_register.lua , marker_stone.lua
mobs
	Renommage de mobs_redo en mobs dans init.lua , mod.conf
mobs_animal
	Modification de mobs_redo en mobs dans depends.txt
serveressentials
	Traduction FR du message ligne 13 dans settings.lua
vehicles
	Supression des vehicules offensifs et des armes dans textures et init.lua


* Mods bonus
!! Attention certains de ces mods ont tendance a ralentir les serveurs !!

biome_lib
	ecosysteme indispensable pour d'autres mods
fire
	pouvoir fabriquer du feu : attention feu !
fishing
	peche : objets (hamecons, ...), animaux (poissons, vers, ...) et concours de peche (sur une periode avec coupes)
	necessite le mod farming
gardening
	fleurs decoratives
jail
	prison : modifier respectivement les coordonnes de la prison et du spawn dans les lignes 7 et 27 du init.lua
lightning
	eclairs : attention feu !
mymonths
	dates, saisons et meteo
plantelife_modpack
	plantes



## EN ##




Notice
------

## FR ##

* Configuration
L'ensemble des options du subgame sont dans le fichier cartOSM/minetest.conf.

* Commandes
La liste des commandes est disponible dans le fichier commandes.md

* Bonus
Les dossiers contenus dans mods-bonus sont des mods supplementaires qui peuvent etre ajoutes au dossier mods (mais qui risquent de ralentir votre serveur).


* Integrer une carte de l'IGN

** Récupérer la carte IGN
Télécharger votre carte IGN depuis :
	http://www.ign.fr/minecraft
en faisant coincider la localisation voulue, votre systeme d'exploitation et votre version de Minetest (idealement 0.4.17).
Un lien vers le fichier a telecharger vous sera envoye par mail entre 1h et 12h apres.

** Depuis l'archive de l'IGN
Decompresser l'archive de l'IGN et la mettre dans votre dossier worlds en la renommant a votre souhait.

Dans le fichier world.mt, changer :
	gameid = minetest
en 
	gameid = cartOSM


** NB
La plupart des blocs de couleur dans les cartes IGN sont en laine et donc facilement inflammables. Attention aux incendies qui ralentissent les ordinateurs.


## EN ##



Compatibility
--------------
This subgame is avalaible with Minetest 0.4.17 and based on Minetest Game [minetest_game] and other mods.
See depends.txt in each mod directory for information about compatibility.


License of source code
----------------------
Copyright (C) 2010-2012 celeron55, Perttu Ahola <celeron55@gmail.com>

See README.txt in each mod directory for information about other authors.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


License of media (textures and sounds)
--------------------------------------
Copyright (C) 2010-2012 celeron55, Perttu Ahola <celeron55@gmail.com>
See README.txt in each mod directory for information about other authors.

Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
http://creativecommons.org/licenses/by-sa/3.0/

License of menu/header.png & menu/icon.png
marpa 2018 (CC BY-SA 3.0)
