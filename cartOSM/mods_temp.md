
Mods
	Avant de manipuler les mods, pensez a verifier les fichiers depends.txt de chacun.

* Mods de minetest_game

** Mods conserves
beds , boats , bones , bucket , carts , creative , doors , dye , farming , flowers , screwdriver , sethome , sfinv , stairs , vessels , walls , wool , xpanes

** Mods modifies
default
	Lave non-inflammable : commentaire des lignes 57 à 66 dans item_entity.lua
give_initial_stuff
	 Ajout de l'inventaire du depart (torches, craftguide, blocs, ...) de la ligne 14 a la ligne 26 dans init.lua
killme
	Traduction FR des messages dans init.lua

** Mods supprimes
fire


* Mods additifs

** Mods ajoutes
areas , arrow_signs , craftguide , display_modpack , h2omes , homedecor , intlib , mapfix , mesecons , moreblocks , moreores , nyancat , polls , signs_lib , simple_skins , spectactor , streets , unifieddyes , unifieddyes_inventory , WorldEdit

** Mods modifies

markers
	Traduction FR dans areas.lua , init.lua , land_title_register.lua , marker_stone.lua
mobs
	Renommage de mobs_redo en mobs dans init.lua , mod.conf
mobs_animal
	Modification de mobs_redo en mobs dans depends.txt
serveressentials
	Traduction FR du message ligne 13 dans settings.lua
vehicles
	Supression des vehicules offensifs et des armes dans textures et init.lua


* Mods bonus
!! Attention certains de ces mods ont tendance a ralentir les serveurs !!

biome_lib
	ecosysteme indispensable pour d'autres mods
fire
	pouvoir fabriquer du feu : attention feu !
fishing
	peche : objets (hamecons, ...), animaux (poissons, vers, ...) et concours de peche (sur une periode avec coupes)
	necessite le mod *farming*
gardening
	fleurs decoratives
lightning
	eclairs : attention feu !
mymonths
	dates, saisons et meteo
plantelife_modpack
	plantes


